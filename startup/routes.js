const express = require('express');

// Loading routes modules
const registrationAPI = require('../routes/api/registration');
const users = require('../routes/api/users');
const contactAPI = require('../routes/api/contact');
const home = require('../routes/web/home');
const about = require('../routes/web/about');
const contact = require('../routes/web/contact');
const registration = require('../routes/web/registration');
//const login = require('../routes/web/login');
const auth = require('../routes/api/auth');
const warehouse = require('../routes/api/warehouse');
const client = require('../routes/api/client');
const employee = require('../routes/api/employee');

module.exports = function(app) {
    app.use('/api/registration', registrationAPI);
    app.use('/api/users', users);
    app.use('/api/contact', contactAPI);
    app.use('/', home);
    app.use('/about', about);
    app.use('/contact', contact);
    app.use('/registration', registration);
    //app.use('/login', login);
    app.use('/api/auth', auth);
    app.use('/api/warehouse', warehouse);
    app.use('/api/client', client);
    app.use('/api/employee', employee);
}