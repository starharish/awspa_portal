const dbPool = require("../startup/mariadb");

async function getRegistrationType() {
    let con;
    let rows = [{id: '', name: '-- Please Select --'}];
    try {
        con = await dbPool.getConnection();
        results = await con.query({rowsAsArray: true, sql: 'SELECT id, name, fee FROM registration_type'});
        
        results.forEach((value, index) => {
            rows[index + 1] = {'id': value[0], 'name': value[1], 'fee': value[2]};
        });
    } catch (error) {
        throw error
    } finally {
        if(con) con.end();
    }

    return rows;
}

exports.getRegistrationType = getRegistrationType;