const Joi = require('joi');
const dbPool = require("../startup/mariadb");

async function getList() {
    let con;
    let rows = [];
    try {
        con = await dbPool.getConnection();
        results = await con.query({rowsAsArray: true, sql: 'SELECT id, name, pan_no, mobile, aadhar_no, address, city, state, pincode, remarks FROM employee ORDER BY created_at DESC'});
        results.forEach((value, index) => {
            rows[index] = {id: value[0], name: value[1], pan_no: value[2], aadhar_no: value[3], mobile: value[4], address: value[5], city: value[6], state: value[7], pincode: value[8], remarks: value[9]};
        }); 
    } catch (error) {
        console.log(error);
    } finally {
        if(con) con.end();
    }

    return rows;
}


async function save(data, company_id) {
    let con;
    let lastId;
    try {
        con = await dbPool.getConnection();
        const result = await con.query('INSERT INTO employee (company_id, name, pan_no, aadhar_no, address, city, state, pincode, mobile, remarks) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', [company_id, data.name, data.pan_no, data.aadhar_no, data.address, data.city, data.state, data.pincode, data.mobile, data.remarks]);
        lastId = result.insertId;

    } catch (err) {
        console.log(err);
    } finally {
        if(con) con.end();
    }

    return lastId;  
}

function validate(req) {
    const schema = {
        name: Joi.string().required(),
        pan_no: Joi.string().required(),
        aadhar_no: Joi.string().required(),
        address: Joi.string().required(),
        city: Joi.string().required(),
        state: Joi.string().required(),
        pincode: Joi.number().integer().required(),
        mobile: Joi.number().integer().required(),
        remarks: Joi.string().required()
    };

    return Joi.validate(req, schema);
}


exports.getEmployeeList = getList;
exports.saveEmployee = save;
exports.validateEmployee = validate;