const dbPool = require("../startup/mariadb");

async function getBusinessType() {
    let con;
    let rows = [{id: '', name: '-- Please Select --'}];
    try {
        con = await dbPool.getConnection();
        results = await con.query({rowsAsArray: true, sql: 'SELECT id, name FROM business_nature'});
        
        results.forEach((value, index) => {
            rows[index + 1] = {'id': value[0], 'name': value[1]};
        });
    } catch (error) {
        throw error
    } finally {
        if(con) con.end();
    }

    return rows;
}

exports.getBusinessType = getBusinessType;