const bcrypt = require('bcryptjs');
const dbPool = require("../startup/mariadb");
const table_name = 'user';
const userInsertQuery = 'INSERT INTO user (email, mobile, login_status_id, is_admin, password, salt) VALUE (?, ?, 4, 0, ?, ?)';
const companyInsertQuery = 'INSERT INTO company (user_id, registration_type_id, company_type_id, business_nature_id, name, website, date_of_incorporation)  VALUE (?, ?, ?, ?, ?, ?, ?)';
const addressQuery = 'INSERT INTO address (company_id, address, city, state, pincode, std_code, telephone, fax, email, address_type) VALUE (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';
const companyProductQuery = 'INSERT INTO company_product (company_id, product_id) VALUE (?, ?)';
const profileQuery = "SELECT u.email AS email, u.mobile AS mobile, c.id AS company_id, c.name AS company_name, c.website AS website, c.date_of_incorporation AS date_of_incorporation, rt.name AS registration_type, ct.name AS company_type, bn.name AS business_nature FROM user AS u INNER JOIN company AS c ON c.user_id = u.id INNER JOIN registration_type AS rt ON rt.id = c.registration_type_id INNER JOIN company_type AS ct ON ct.id = c.company_type_id INNER JOIN business_nature AS bn ON bn.id = c.business_nature_id WHERE u.id = ?";


async function register(data) {
    let con;
    let rows = {};
    try {
        con = await dbPool.getConnection();
        // insert in user tabel
        let salt = await bcrypt.genSalt(10);
        let hash = await bcrypt.hash("123456", salt);
        let userResult = await con.query(userInsertQuery, [data.email, data.mobile, hash, salt]);

        rows['user'] = userResult;
        let userId = userResult.insertId;

        // insert in company
        let companyResult = await con.query(companyInsertQuery, [userId, data.registartion_type, data.company_type, 
            data.business_type, data.company_name, data.website, data.date_of_incorporation]);
        let companyId = companyResult.insertId;
        rows['company'] = companyResult;

        // insert in company prduct
        let productResults = [];
        for(let i = 0; i < data.product.length; i++) {
            productResults[i] = await con.query(companyProductQuery, [companyId, data.product[i]]);
        }

        rows['products'] = productResults;
        
        // insert in address
        let regAddressResult = await con.query(addressQuery, [companyId, data.reg_address, data.reg_city, data.reg_state,
        data.reg_pincode, data.reg_std_code, data.reg_telephone, data.reg_fax, data.reg_email, data.reg_type]); 
        rows['reg_address'] = regAddressResult;

        let corpAddressResult = await con.query(addressQuery, [companyId, data.corp_address, data.corp_city, data.corp_state,
            data.corp_pincode, data.corp_std_code, data.corp_telephone, data.corp_fax, data.corp_email, data.corp_type]); 
        rows['corp_address'] = corpAddressResult;

        let comAddressResult = await con.query(addressQuery, [companyId, data.com_address, data.com_city, data.com_state,
            data.com_pincode, data.com_std_code, data.com_telephone, data.com_fax, data.com_email, data.com_type]);    
        rows['com_address'] = comAddressResult;    


    } catch (error) {
        console.log(error);
    } 

    return rows;
}

async function registerOLD(data) {
    let con;
    let userId;
    let rows = {};
    try{
        con = await dbPool.getConnection();
        // insert in user tabel
        let salt = await bcrypt.genSalt(10);
        let hash = await bcrypt.hash("123456", salt);
        let userResult = await con.query(userInsertQuery, [data.email, data.mobile, hash, salt]);
        userId = userResult.insertId;
                                  
        // insert in company
        let companyResult = await con.query(companyInsertQuery, [userId, data.registartion_type, data.company_type, 
            data.business_type, data.company_name, data.website, data.date_of_incorporation]);
        let companyId = companyResult.insertId;
        
        // insert in company prduct
        for(let i = 0; i < data.product.length; i++) {
            await con.query(companyProductQuery, [companyId, data.product[i]]);
        }
        
        // insert in address
        let regAddressResult = await con.query(addressQuery, [companyId, data.reg_address, data.reg_city, data.reg_state,
        data.reg_pincode, data.reg_std_code, data.reg_telephone, data.reg_fax, data.reg_email, data.reg_type]); 
        
        let corpAddressResult = await con.query(addressQuery, [companyId, data.corp_address, data.corp_city, data.corp_state,
            data.corp_pincode, data.corp_std_code, data.corp_telephone, data.corp_fax, data.corp_email, data.corp_type]); 

        let comAddressResult = await con.query(addressQuery, [companyId, data.com_address, data.com_city, data.com_state,
            data.com_pincode, data.com_std_code, data.com_telephone, data.com_fax, data.com_email, data.com_type]);    

        rows = {user_id: userId};    
        console.log(rows);
    } catch (err) {
        throw err;
    } finally {
        if(con) return con.end();
    }
    
    return rows;
}

async function getList() {
    let con;
    let rows;
    try {
        con = await dbPool.getConnection();
        rows = await con.query('SELECT * FROM user');        
    } catch(err) {
        console.log(err);
    } finally {
        con.end();
    }

    return rows;
}

async function findUserByEmail(email) {
    let con;
    let rows = {};
    try {
        con = await dbPool.getConnection();
        let userResult = await con.query('SELECT * FROM user WHERE email=? LIMIT 1', [email]);   
        userResult.forEach((value, index) => {
            rows = value;
        });    
    } catch(err) {
        console.log(err);
    } finally {
        con.end();
    }

    return rows;
}

async function getProfile(userId) {
    let con;
    let rows = {};
    try {
        con = await dbPool.getConnection();
        const profileResults = await con.query(profileQuery, [userId]);
        //console.log(profileResults);
        profileResults.forEach((value, index) => {
            rows['profile'] = value;
        });

        let address = [];   
        addressResults = await con.query('SELECT * FROM address WHERE company_id = ?', [rows.profile.company_id]);
        addressResults.forEach((value, index) => {
            address[index] = value;
        });

        rows['address'] = address;

        let products = [];
        companyProductsResults = await con.query('SELECT p.name FROM company_product AS cp INNER JOIN product AS p ON p.id = cp.product_id WHERE cp.company_id = ?', [rows.profile.company_id]);
        companyProductsResults.forEach((value, index) => {
            products[index] = value;
        });

        rows['products'] = products;
        
    } catch (err) {
        console.log(err);
    } finally {
        con.end();
    }

    return rows;
}

// validate registartion
function validate() {
    
}


exports.registerUser = register;
exports.getList = getList;
exports.getProfile = getProfile;
exports.findUserByEmail = findUserByEmail;