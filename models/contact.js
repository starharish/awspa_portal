const Joi = require('joi');
const dbPool = require("../startup/mariadb");

async function save(data) {
    let con;
    let lastId;
    try {
        con = await dbPool.getConnection();
        const result = await con.query('INSERT INTO contact (first_name, last_name, email, message) VALUES(?, ?, ?, ?)', [data.first_name, data.last_name, data.email, data.message]);
        return result;

    } catch (err) {
        console.log(err);
    } finally {
        if(con) con.end();
    }

    return lastId;   
}


function validate(req) {
    const schema = {
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        email: Joi.string().required().email(),
        message: Joi.string().required(),
    };

    return Joi.validate(req, schema);
}

exports.saveContact = save;
exports.validateContact = validate;