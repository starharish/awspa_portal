const dbPool = require("../startup/mariadb");

async function getIdByUser(user_id) {
    let con;
    let company_id = 0;
    try {
        con = await dbPool.getConnection();
        results = await con.query({rowsAsArray: true, sql: 'SELECT id FROM company WHERE user_id = ?'}, [user_id]);
        
        results.forEach((value) => {
            company_id = value[0];
        });
        
    } catch (error) {
        console.log(error);
    } finally{
        if(con) con.end();
    }
    
    return company_id;
}


exports.getCompanyId = getIdByUser;