$(document).ready(function () {
	var addressNameList = ['address', 'city', 'pincode', 'state', 'std_code', 'telephone', 'fax', 'email'];

	$('#contactUsForm').validate({
		submitHandler: function(form) {
			var formArray = $(form).serializeArray();
			var formData = {};
			$.each(formArray, function(index, value) {
				formData[value.name] = value.value;
			});

			$.ajax({
				type: 'post',
				url: '/api/contact',
				data: JSON.stringify(formData),
				dataType: 'json',
				contentType: 'application/json',
				success: function (data) {
					//if(data.status === 1) {
						$(form)[0].reset();
						alert('Submitted successfully.');
					//}
				}
			});
		}
	});

	$('#same_reg_check').on('change', function(){
		$.each(addressNameList, function(i, v) {
			var elmValue = $('#reg_'+v).val();		
			$('#corp_'+v).val(elmValue);
		});
	});

	$('#corporate_same_reg_check').on('change', function(){
		$.each(addressNameList, function(i, v) {
			var elmValue = $('#reg_'+v).val();		
			$('#com_'+v).val(elmValue);
		});
	});

	$('#corporate_same_corp_check').on('change', function(){
		$.each(addressNameList, function(i, v) {
			var elmValue = $('#corp_'+v).val();		
			$('#com_'+v).val(elmValue);
		});
	});


	$("#registrationForm").validate({
		submitHandler: function(form) {
			var formArray = $(form).serializeArray();
			var formData = {};
			var product = [];
			$.each(formArray, function(index, value) {
				if(value.name === 'product') {
					product.push(parseInt(value.value));
				} else {
					formData[value.name] = (!isNaN(value.value)) ? parseInt(value.value): value.value;
				}
			});

			formData['product'] = product;
			
			$.ajax({
				type: 'post',
				url: '/api/registration',
				data: JSON.stringify(formData),
				dataType: 'json',
				contentType: 'application/json',
				success: function (data) {
					// res = JSON.parse(data);
					//if(data.status === 1) {
						$(form)[0].reset();
						window.location = '/registration/success?rid='+data.regId;
					//}
				}
			});
		}
	});
	
});

var siteCarousel = function () {
	if ( $('.nonloop-block-13').length > 0 ) {
		$('.nonloop-block-13').owlCarousel({
		center: false,
		items: 1,
		loop: true,
			stagePadding: 0,
		margin: 0,
		autoplay: true,
		nav: true,
			navText: ['<span class="icon-arrow_back">', '<span class="icon-arrow_forward">'],
		responsive:{
		600:{
			margin: 0,
			nav: true,
			items: 2
		},
		1000:{
			margin: 0,
			stagePadding: 0,
			nav: true,
			items: 2
		},
		1200:{
			margin: 0,
			stagePadding: 0,
			nav: true,
			items: 3
		}
		}
		});
	}

	$('.slide-one-item').owlCarousel({
	center: false,
	items: 1,
	loop: true,
		stagePadding: 0,
	margin: 0,
	smartSpeed: 1500,
	autoplay: true,
	pauseOnHover: false,
	dots: true,
	nav: true,
	navText: ['<span class="icon-keyboard_arrow_left">', '<span class="icon-keyboard_arrow_right">']
	});
};
siteCarousel();

$('#loginForm').validate({
	submitHandler: function(form) {
		var formArray = $(form).serializeArray();
		var formData = {};
		$.each(formArray, function(index, value) {
			formData[value.name] = value.value;
		});

		console.log(formData);

		$.ajax({
			type: 'post',
			url: '/api/auth',
			data: JSON.stringify(formData),
			dataType: 'json',
			contentType: 'application/json',
			success: function (data) {
				//if(data.status === 1) {
					$(form)[0].reset();
					alert('Submitted successfully.');
				//}
			}
		});
	}
});