import React, { Component } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
} from 'reactstrap';

import {
  Redirect
} from "react-router-dom";

import $ from 'jquery';
import config from 'react-global-configuration';

class Add extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
          redirect: false
        };
    }

    handleInput = (event) => {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;

      this.setState({
        [name] : value
      });
    };

    handleSubmit = (event) => {
      event.preventDefault();
      let isValid = true;
      $('.invalid-feedback').hide();
      if(typeof this.state.name === 'undefined') {
        $('#name-invalid').show();
        isValid = false;
      }

      if(typeof this.state.pan_no === 'undefined') {
        $('#pan_no-invalid').text('Please enter Pan No.').show();

        isValid = false;
      } else if(/(^([a-zA-Z]{5})([0-9]{4})([a-zA-Z]{1})$)/.test(this.state.pan_no) === false) {
        $('#pan_no-invalid').text('Please enter a valid Pan No.').show();
        isValid = false;
      }

      if(typeof this.state.aadhar_no === 'undefined') {
        $('#aadhar_no-invalid').text('Please enter Aadhar No.').show();
        isValid = false;
      } else if(/^\d{4}\d{4}\d{4}$/.test(this.state.aadhar_no) === false) {
        $('#aadhar_no-invalid').text('Please enter a valid Aadhar No.').show();
        isValid = false;
      }

      if(typeof this.state.address === 'undefined') {
        $('#address-invalid').show();
        isValid = false;
      }

      if(typeof this.state.city === 'undefined') {
        $('#city-invalid').show();
        isValid = false;
      }

      if(typeof this.state.state === 'undefined') {
        $('#state-invalid').show();
        isValid = false;
      }

      if(typeof this.state.pincode === 'undefined') {
        $('#pincode-invalid').text('Please enter Pincode.').show();
        isValid = false;
      } else if(/^[1-9][0-9]{5}$/.test(this.state.pincode) === false) {
        $('#pincode-invalid').text('Please enter a valid Pincode.').show();
        isValid = false;
      }

      if(typeof this.state.remarks === 'undefined') {
        $('#remarks-invalid').show();
        isValid = false;
      }

      if(isValid) {        
        $.ajax({
          type: 'post',
          beforeSend: function(request) {
            var token = localStorage.getItem('token');
            request.setRequestHeader("x-auth-token", token);
          },
          url: config.get("api_url")+'/api/warehouse',
          data: JSON.stringify({name: this.state.name, pan_no: this.state.pan_no, aadhar_no: this.state.aadhar_no,
          address: this.state.address, city: this.state.city, state: this.state.state,
          pincode: this.state.pincode, remarks: this.state.pincode}),
          dataType: 'json',
          contentType: 'application/json',
          success: function (data) {
            if(data.status === 1) {
              $('#redirect').trigger('click');
            }
          },
          statusCode: {
            400: function() {
              localStorage.removeItem('token')
              $('#redirect').trigger('click');
            },
            403: function() {
              localStorage.removeItem('token')
              $('#redirect').trigger('click');
            }
          }
        });
        
      }
      
    };

    redirectToView = () => {
      this.setState({ redirect: true });
    };

    render() {
      const { redirect } = this.state;

      if (redirect) {
        return <Redirect to='/warehouse/view'/>;
      }

        return(
        <div className="animated fadeIn">
        <form method="post" onSubmit={this.handleSubmit}>
        <Row>
          <Col xs="12" sm="6">
            <Card>
              <CardHeader>
                <strong>Add Warehouse</strong>
                <small> </small>
              </CardHeader>
              <CardBody>
                <FormGroup>
                  <Label htmlFor="name">Name</Label>
                  <Input type="text" id="name" placeholder="" name="name" onChange={this.handleInput} />
                  <div className="invalid-feedback" id="name-invalid">
                    Please enter a name.
                  </div>
                </FormGroup>
                <FormGroup row className="my-0">
                  <Col xs="6">
                    <FormGroup>
                      <Label htmlFor="pan_no">Pan No</Label>
                      <Input type="text" id="pan_no" placeholder="" name="pan_no" onChange={this.handleInput} />
                      <div className="invalid-feedback" id="pan_no-invalid">
                        Please enter Pan No.
                      </div>
                    </FormGroup>
                  </Col>
                  <Col xs="6">
                    <FormGroup>
                      <Label htmlFor="aadhar_no">Aadhar No</Label>
                      <Input type="text" id="aadhar_no" placeholder="" name="aadhar_no" onChange={this.handleInput} />
                      <div className="invalid-feedback" id="aadhar_no-invalid">
                        Please enter Aadhar No.
                      </div>
                    </FormGroup>
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="address">Address</Label>
                  <Input type="text" id="address" placeholder="" name="address" onChange={this.handleInput} />
                  <div className="invalid-feedback" id="address-invalid">
                    Please enter Address.
                  </div>
                </FormGroup>
                <FormGroup row className="my-0">
                  <Col xs="6">
                    <FormGroup>
                      <Label htmlFor="city">City</Label>
                      <Input type="text" id="city" placeholder="" name="city" onChange={this.handleInput} />
                      <div className="invalid-feedback" id="city-invalid">
                        Please enter City.
                      </div>
                    </FormGroup>
                  </Col>
                  <Col xs="6">
                    <FormGroup>
                      <Label htmlFor="state">State</Label>
                      <Input type="text" id="state" placeholder="" name="state" onChange={this.handleInput} />
                      <div className="invalid-feedback" id="state-invalid">
                        Please enter State.
                      </div>
                    </FormGroup>
                  </Col>
                </FormGroup>
                <FormGroup row className="my-0">
                  <Col xs="6">
                    <FormGroup>
                      <Label htmlFor="pincode">Pincode</Label>
                      <Input type="text" id="pincode" placeholder="" name="pincode" onChange={this.handleInput} />
                      <div className="invalid-feedback" id="pincode-invalid">
                        Please enter Pincode.
                      </div>
                    </FormGroup>
                  </Col>
                  <Col xs="6">
                    <FormGroup>
                      
                    </FormGroup>
                  </Col>
                </FormGroup>
                <FormGroup>
                  <Label htmlFor="remarks">Remarks</Label>
                  <Input type="text" id="remarks" placeholder="" name="remarks" onChange={this.handleInput} />
                  <div className="invalid-feedback" id="remarks-invalid">
                    Please enter Remark.
                  </div>
                  <Input type="hidden" id="redirect" onClick={this.redirectToView} />
                </FormGroup>
                <FormGroup className="form-actions">
                  <Button type="submit" size="sm" color="primary">Submit</Button>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
        </form>
      </div>
        );
    }
}

export default Add;
