import React, { Component, lazy, Suspense, Fragment } from 'react';
import { render} from 'react-dom';
import { Redirect } from 'react-router-dom';
import { Input } from 'reactstrap';
import $ from 'jquery';
import ReactDatatable from '@ashvin27/react-datatable';
import config from 'react-global-configuration';


class Employee extends Component {
    constructor(props) {
       super(props);
       this.columns = [
        {
            key: "name",
            text: "Name",
            className: "name",
            align: "left",
            sortable: true,
        },
        {
            key: "pan_no",
            text: "PAN No",
            className: "pan_no",
            align: "left",
            sortable: true,
        },
        {
            key: "aadhar_no",
            text: "Aadhar No",
            className: "aadhar_no",
            align: "left",
            sortable: true
        },
        {
            key: "mobile",
            text: "Mobile",
            className: "mobile",
            align: "left",
            sortable: true
        },
        {
            key: "address",
            text: "Address",
            className: "address",
            sortable: true
        },
        {
            key: "city",
            text: "City",
            className: "city",
            align: "left",
            sortable: true
        },
        {
            key: "state",
            text: "State",
            className: "state",
            sortable: true,
            align: "left"
        },
        {
            key: "pincode",
            text: "Pincode",
            className: "pincode",
            sortable: true,
            align: "left"
        },
        {
            key: "remarks",
            text: "Remarks",
            className: "remarks",
            sortable: true,
            align: "left"
        },
        {
            key: "action",
            text: "Action",
            className: "action",
            width: 100,
            align: "left",
            sortable: false,
            cell: record => { 
                return (
                    <Fragment>
                        <button
                            className="btn btn-primary btn-sm"
                            onClick={() => this.editRecord(record)}
                            style={{marginRight: '5px'}}>
                            <i className="fa fa-edit"></i>
                        </button>
                        <button 
                            className="btn btn-danger btn-sm" 
                            onClick={() => this.deleteRecord(record)}>
                            <i className="fa fa-trash"></i>
                        </button>
                    </Fragment>
                );
            }
        }
    ];

    this.config = {
        page_size: 10,
        length_menu: [ 10, 20, 50 ],
        button: {
            excel: true,
            print: true
        },
        sort: {column: "id", order: "desc"}
    }
    
    this.state = {
        records: []
    };

    }

    componentDidMount() {
        var token = localStorage.getItem('token');

        fetch(config.get("api_url")+"/api/employee", {
            headers: {
               'x-auth-token': token,
            }
        })
        .then(
            (response) => {
                if (response.status === 400 || response.status === 403) {
                    localStorage.removeItem('token');
                    $('#redirect').trigger('click');
                    return;
                }
            
                // Examine the text in the response
                response.json().then((data) => {
                    this.setState({
                        records: data
                    });
                });
            }
        )
    }

    redirectToView = () => {
        this.setState({ redirect: true });
    };

    render() {
        const { redirect } = this.state;

        if (redirect) {
            return <Redirect to='/dashboard'/>;
        }

        return (
            <div>
                <ReactDatatable
                    config={this.config}
                    records={this.state.records}
                    columns={this.columns}
                />

                <Input type="hidden" id="redirect" onClick={this.redirectToView} />
            </div>
        );
    }
}

export default Employee;