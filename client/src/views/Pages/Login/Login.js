import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import $ from 'jquery';
import config from 'react-global-configuration';

class Login extends Component {
  constructor(props) {
      super(props);
      
      this.state = {
        redirect: false
      };
  }


  handleInput = (event) => {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name] : value
    });
  };

  submitHandler = (event) => {
    event.preventDefault();
    let isValid = true;
    $('.invalid-feedback').hide();

    if(typeof this.state.email === 'undefined') {
      $('#email-invalid').text('Please enter a Email.').show();
      isValid = false;
    }
    // } else if(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(this.state.email)) {
    //   $('#email-invalid').text('Please enter a vaild Email.').show();
    //   isValid = false;
    // }

    if(typeof this.state.password === 'undefined') {
      $('#password-invalid').show();
      isValid = false;
    }

    if(isValid) {
      $.ajax({
        type: 'post',
        url: config.get("api_url")+'/api/auth',
        data: JSON.stringify({email: this.state.email, password: this.state.password}),
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
          //if(data.status === 1) {
            localStorage.setItem('token', data.token);
            $('#redirect').trigger('click');
          //}
        },
        statusCode: {
          400: function() {
            $('#email-invalid').text('Invalid Email or Password.').show();
          }
        }
      });
    }
    
  }

  componentDidMount() {
    var token = localStorage.getItem('token');
    if(token) {
      $('#redirect').trigger('click');
    }
  }

  redirectToView = () => {
    this.setState({ redirect: true });
  };

  render() {
    const { redirect } = this.state;

    if (redirect) {
      return <Redirect to='/dashboard'/>;
    }

    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="8">
              <CardGroup>
                <Card className="p-4">
                  <CardBody>
                    <Form method="post" onSubmit={this.submitHandler}>
                      <h1>Login</h1>
                      <p className="text-muted">Sign In to your account</p>
                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="email" placeholder="Email" autoComplete="Email" name="email" onChange={this.handleInput} />
                        <div className="invalid-feedback" id="email-invalid">
                          Please enter a Email.
                        </div>
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input type="password" placeholder="Password" autoComplete="current-password" name="password" onChange={this.handleInput}  />
                        <div className="invalid-feedback" id="password-invalid">
                          Please enter a Password.
                        </div>
                      </InputGroup>
                      <Row>
                        <Col xs="6">
                          <Button color="primary" className="px-4">Login</Button>
                        </Col>
                        <Col xs="6" className="text-right">
                          {/* <Button color="link" className="px-0">Forgot password?</Button> */}
                          <Input type="hidden" id="redirect" onClick={this.redirectToView} />
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

export default Login;
