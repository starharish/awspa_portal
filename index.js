const config = require('config');
const express = require('express');
const path = require('path');
const cors = require('cors');

const app = express();

app.use(cors());

// setting template engine
app.set('view engine', 'pug');

// load static resources
app.use(express.static(path.join(__dirname, 'views')));



// STARTUP
require("./startup/routes")(app);

// Serve the static files from the React app
app.use(express.static(path.join(__dirname, 'client/build')));

// Handles any requests that don't match the ones above
app.get('/profile', (req,res) => {
    // LOAD THE REACT APP
    //res.send("in profile");
    res.sendFile(path.join(__dirname+'/client/build/index.html'));
});

const port = config.has('server.port') ? config.get('server.port') : 0;

if(!port) {
    console.log("No port configured.");
    return;
}

app.listen(port, () => {
    console.log("App listening on.", port);
});
