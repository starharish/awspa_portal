DROP TABLE IF EXISTS `registration_type`;
CREATE TABLE `registration_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `fee` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `registration_type` VALUES (1,'full member',10000, '2019-07-19 10:55:06','2019-07-19 10:55:06'),(2,'associate member', 5000,'2019-07-19 10:55:40','2019-07-19 10:55:40'),(3,'affiliate member', 1000,'2019-07-19 10:56:16','2019-07-19 10:56:16'),(4,'foreign affiliate	 member', 100,'2019-07-19 10:57:03','2019-07-19 10:57:03');

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB

CREATE TABLE `company_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `company_type` VALUES (1,'public ltd','2019-07-19 11:05:27','2019-07-19 11:05:27'),(2,'private ltd','2019-07-19 11:05:41','2019-07-19 11:05:41'),(3,'partnership','2019-07-19 11:05:49','2019-07-19 11:05:49'),(4,'proprietorship','2019-07-19 11:05:56','2019-07-19 11:05:56'),(5,'co-operative','2019-07-19 11:06:05','2019-07-19 11:06:05'),(6,'psu','2019-07-19 11:06:10','2019-07-19 11:06:10');

CREATE TABLE `business_nature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `business_nature` VALUES (1,'Processor/manufacturer & Explorer','2019-07-19 11:08:34','2019-07-19 11:08:34'),(2,'Merchant Explorer','2019-07-19 11:08:50','2019-07-19 11:08:50'),(3,'Refiner','2019-07-19 11:09:02','2019-07-19 11:09:02'),(4,'Surveycr/Inspection Agency','2019-07-19 11:09:22','2019-07-19 11:09:22'),(5,'Analyst/Certification Agency','2019-07-19 11:09:35','2019-07-19 11:09:35'),(6,'Shipping Agent/CHA','2019-07-19 11:09:47','2019-07-19 11:09:47'),(7,'Foreign Buyer','2019-07-19 11:10:00','2019-07-19 11:10:00'),(8,'Foreign Buyer\'s Agent','2019-07-19 11:10:41','2019-07-19 11:10:41');

CREATE TABLE `login_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

INSERT INTO `login_status` VALUES (1,'approved','2019-07-19 10:18:45','2019-07-19 10:18:45'),(2,'rejected','2019-07-19 10:19:02','2019-07-19 10:19:02'),(3,'cancelled','2019-07-19 10:19:20','2019-07-19 10:19:20'),(4,'pending','2019-07-19 10:20:15','2019-07-19 10:20:15');

CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `login_status_id` int(11) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `user_login_status` (`login_status_id`),
  CONSTRAINT `user_login_status` FOREIGN KEY (`login_status_id`) REFERENCES `login_status` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `registration_type_id` int(11) NOT NULL,
  `company_type_id` int(11) NOT NULL,
  `business_nature_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `date_of_incorporation` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `company_user_id` (`user_id`),
  KEY `company_registration_type_id` (`registration_type_id`),
  KEY `company_type` (`company_type_id`),
  KEY `company_business_nature_id` (`business_nature_id`),
  CONSTRAINT `company_business_nature_id` FOREIGN KEY (`business_nature_id`) REFERENCES `business_nature` (`id`),
  CONSTRAINT `company_registration_type_id` FOREIGN KEY (`registration_type_id`) REFERENCES `registration_type` (`id`),
  CONSTRAINT `company_type` FOREIGN KEY (`company_type_id`) REFERENCES `company_type` (`id`),
  CONSTRAINT `company_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `company_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `product_company_id` (`company_id`),
  KEY `company_product_id` (`product_id`),
  CONSTRAINT `company_product_id` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`),
  CONSTRAINT `product_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `pincode` varchar(10) NOT NULL,
  `std_code` varchar(3) NOT NULL,
  `telephone` varchar(10) NOT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address_type` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `address_company_id` (`company_id`),
  CONSTRAINT `address_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
  ) ENGINE=InnoDB;

  CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pan_no` varchar(10) NOT NULL,
  `aadhar_no` varchar(12) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `pincode` varchar(10) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `remarks` text NOT NULL,
  `is_blacklisted` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `client_company_id` (`company_id`),
  CONSTRAINT `client_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `warehouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pan_no` varchar(10) NOT NULL,
  `aadhar_no` varchar(12) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `pincode` varchar(10) NOT NULL,
  `remarks` text NOT NULL,
  `is_blacklisted` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `warehouse_company_id` (`company_id`),
  CONSTRAINT `warehouse_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pan_no` varchar(10) NOT NULL,
  `aadhar_no` varchar(12) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `pincode` int(6) NOT NULL,
  `mobile` varchar(10) NOT NULL,
  `remarks` text NOT NULL,
  `is_blacklisted` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `employee_company_id` (`company_id`),
  CONSTRAINT `employee_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`)
) ENGINE=InnoDB;

INSERT INTO `awspa_portal`.`product` (`name`) VALUES ('oil');
INSERT INTO `awspa_portal`.`product` (`name`) VALUES ('wheat');
INSERT INTO `awspa_portal`.`product` (`name`) VALUES ('rice');
INSERT INTO `awspa_portal`.`product` (`name`) VALUES ('cloths');


CREATE TABLE `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



