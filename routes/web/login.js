const express = require('express');
const pug = require('pug');

const router = express.Router();

router.get('/', (req, res) => {
    let menu = [{name: 'Home', url: '/'}, {name: 'Contact', url: '/contact'}, {name: 'About', url: '/about'}];

    res.render('login', {nav_bar: menu});
});


module.exports = router;