const express = require('express');
const pug = require('pug');
const {getRegistrationType} = require('../../models/registration_type');
const {getProfile} = require('../../models/user');
const {getCompanyType} = require('../../models/company_type');
const {getBusinessType} = require('../../models/business_nature');
const {getProduct} = require('../../models/product');

const router = express.Router();

router.get('/', async (req, res) => {
    let menu = [{name: 'Home', url: '/'}, {name: 'Contact', url: '/contact'}, {name: 'About', url: '/about'}, {name: 'Login', url: '/profile'}];
    
    // Master list data
    let registrationType = await getRegistrationType();
    let companyType = await getCompanyType();
    let businessType = await getBusinessType();
    let product = await getProduct();

    res.render('registration', {title: "AWSPA - Home", 
        nav_bar: menu, 
        registrationType: registrationType,
        companyType: companyType,
        businessType: businessType,
        product: product
    });
});

router.get('/success', async (req, res) => {
    let menu = [{name: 'Home', url: '/'}, {name: 'Contact', url: '/contact'}, {name: 'About', url: '/about'}];
    const userId = req.query.rid;
    const userData = await getProfile(userId);
    userData['nav_bar'] = menu;
    res.render('registration_success', userData);
});

module.exports = router;