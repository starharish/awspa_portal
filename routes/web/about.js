const express = require('express');
const pug = require('pug');

const router = express.Router();

router.get('/', (req, res) => {
    let menu = [{name: 'Home', url: '/'}, {name: 'Contact', url: '/contact'}, {name: 'About', url: '/about'}, {name: 'Registration', url: '/registration'}, {name: 'Login', url: '/profile'}];
    res.render('about', {title: "AWSPA - About", nav_bar: menu});
});


module.exports = router;