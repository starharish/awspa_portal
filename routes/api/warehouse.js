const express = require('express');
const router = express.Router();

const {getWarehouseList, saveWarehouse, validateWarehouse} = require('../../models/warehouse');
const auth = require("../../middleware/auth");

router.use(express.json());
router.use(express.urlencoded({extended: false}));

router.get('/', auth, async (req, res) => {
    //res.status(400).json({}); return;
    const warehouse = await getWarehouseList();
    res.json(warehouse);
});

router.post('/', auth, async (req, res) => {
    const {error} = validateWarehouse(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    try{
        let lastInsertId = await saveWarehouse(req.body, req.user.company_id);
        if(lastInsertId) {
            res.json({status: 1, id: lastInsertId});
            return;
        } 
    } catch(err) {
        console.log(err);
    }

    res.status(500).json({status: 0});
});

module.exports = router;