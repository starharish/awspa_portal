const express = require('express');
const Joi = require('joi');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');
const _ = require('lodash');

const {findUserByEmail} = require('../../models/user');
const {getCompanyId} = require('../../models/company');

const router = express.Router();

router.use(express.json());
router.use(express.urlencoded({extended: false}));

router.post('/', async (req, res) => {

    const { error } = validate(req.body); 
    if (error) return res.status(400).send(error.details[0].message);

    let user = await findUserByEmail(req.body.email);

    if (!user) return res.status(400).send('Invalid email or password.');

    let password = user.password;
   
    const validPassword = await bcrypt.compare(req.body.password, password);

    if (!validPassword) return res.status(400).send('Invalid email or password.');

    // fetch company id for the given user
    let user_id = user.id;
    let company_id = await getCompanyId(user_id);  

    const token = jwt.sign(
        {
            id: user.id,
            email: user.email,
            is_admin: user.is_admin,
            company_id: company_id
        },
        config.get("jwtPrivateKey")
    );
    

    res.header("x-auth-token", token).send({
        id: user.id,
        email: user.email,
        is_admin: user.is_admin,
        token: token
    });
});

function validate(req) {
    const schema = {
        email: Joi.string().required().email(),
        password: Joi.string().min(5).max(255).required()
    };

    return Joi.validate(req, schema);
}

module.exports = router;