const express = require('express');
const {saveContact, validateContact} = require('../../models/contact');

const router = express.Router();

router.use(express.json());
router.use(express.urlencoded({extended: false}));

router.post('/', (req, res) => {
    const {error} = validateContact(req.body);
    if(error) return res.status(400).send(error.details[0].message);

    let lastInsertId = saveContact(req.body);
    if(lastInsertId) {
        res.send({status: 1, id: lastInsertId});
        return;
    } 

    res.status(500).send({status: 0});
});

module.exports = router;