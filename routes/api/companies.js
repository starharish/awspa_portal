const express = require('express');
const router = express.Router();

/**
 * get the user data
 */
router.get('/me', (req, res) => {
    res.send("Company info");
});

module.exports = router;