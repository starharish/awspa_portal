const express = require('express');
const _ = require('lodash');
const {registerUser} = require('../../models/user');

const router = express.Router();

const fields = ['email', 'mobile', 'registartion_type', 'company_name', 'company_type', 'business_type',
'website', 'date_of_incorporation', 'product', 'reg_address', 'reg_city', 'reg_state', 'reg_pincode',
'reg_std_code', 'reg_telephone', 'reg_fax', 'reg_email', 'reg_type', 'corp_address', 'corp_city', 'corp_state',
'corp_pincode', 'corp_std_code', 'corp_telephone', 'corp_fax', 'corp_email', 'corp_type',
'com_address', 'com_city', 'com_state', 'com_pincode',
'com_std_code', 'com_telephone', 'com_fax', 'com_email', 'com_type'];


router.use(express.json());
router.use(express.urlencoded({extended: false}));


router.post('/', async (req, res) => {
    let data = _.pick(req.body, fields);
   
    const regResults = await registerUser(data);
    res.json({regId: regResults.user.insertId, status: '1'});
});



module.exports = router;