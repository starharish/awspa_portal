const express = require('express');
const router = express.Router();

const {getEmployeeList, saveEmployee, validateEmployee} = require('../../models/employee');
const auth = require("../../middleware/auth");

router.use(express.json());
router.use(express.urlencoded({extended: false}));

router.get('/', async (req, res) => {
    const employee = await getEmployeeList();
    res.json(employee);
});

router.post('/', auth, async (req, res) => {
    const {error} = validateEmployee(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    try{
        let lastInsertId = await saveEmployee(req.body, req.user.company_id);
        if(lastInsertId) {
            res.json({status: 1, id: lastInsertId});
            return;
        } 
    } catch(err) {
        console.log(err);
    }

    res.status(500).json({status: 0});
});

module.exports = router;