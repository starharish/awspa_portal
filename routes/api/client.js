const express = require('express');
const router = express.Router();

const {getClientList, saveClient, validateClient} = require('../../models/client');
const auth = require("../../middleware/auth");

router.use(express.json());
router.use(express.urlencoded({extended: false}));

router.get('/', auth, async (req, res) => {
    const client = await getClientList();
    res.status(200).json(client);
});

router.post('/', auth, async (req, res) => {
    const {error} = validateClient(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    
    try{
        let lastInsertId = await saveClient(req.body, req.user.company_id);
        if(lastInsertId) {
            res.json({status: 1, id: lastInsertId});
            return;
        } 
    } catch(err) {
        console.log(err);
    }

    res.status(500).json({status: 0});
});

module.exports = router;