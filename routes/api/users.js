const express = require('express');
const {getList} = require('../../models/user');

const router = express.Router();

/**
 * get the user data
 */
router.get('/me', (req, res) => {
    const userList = getList();
    res.send(userList);
});

/**
 * Register the user
 */
router.post('/', (req, res) => {
    res.send(req.body);
});


module.exports = router;